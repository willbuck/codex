# WHAT

This repository implements a computerized judge for the board/card game [Codex](http://www.sirlin.net/codex) by David Sirlin.

While the long-term intended use is to provide a baseline for an interactive implementation for realtime online multiplayer, we also present an offline adjudicator that takes a board state and sequence of plays and outputs the modified board state.

# WHY

I want to wire up an online Codex judge/matchmaker, but before I can even pretend to actually do such a thing, I should make sure that I can handle game effects and interactions.

The easiest way to do so is to whip up a quick test runner that makes it:

- easy to define arbitrary board state
- straightforward to issue actions
- easy to query the resultant boardstate

For this project to be generally useful, these tests should be language-agnostic (I'm going with YAML for board state, and a simple keyword parser for actions). This will allow anyone to write a native desktop/mobile application, for instance, by taking the test cases wholesale and only needing to port the game logic.

# HOW

```
$ bundle install
$ bundle exec rake spec
```

TODO: Figure out some commandline usage to run it outside specs.


# WIP

This is very much a work-in-progress, with a tiered development plan.

## PHASE ONE

Allow 1v1 games with any of the seven starting decks, with:

- NO Heroes
- NO Spells
- NO Tech Buildings
- NO Addons
- NO Teching cards

The game is broken at this point (dead cards; worker 5 times, draw your whole deck each turn) but that doesn't matter - the important thing is that all the mechanics required to play all 42 starting deck creatures is in place, including activated abilities and ability targeting, and that under these constraints we can run through a full game from start to base death.

## PHASE TWO

Handle 1v1 games with heroes.

This will add support for both hero abilities and the casting of minor spells.

## PHASE THREE

Handle 1v1 games with higher tech.

This will add support for constructing tech buildings, drafting cards in the Tech phase, and all remaining spells and unit keywords/abilities.

## PHASE FOUR

Handle 3v3 games.

This will add support for declaring multiple specs, spec choice at Tech II, multiple hero behaviour, and is a good time to also work in addons.

## PHASE FIVE

Map cards. If I ever actually get this far I'll be seriously impressed with myself.

# Currently Supported/Specified Features

_Planned features for the current development phase display checkboxes_

## Game Phases/Actions

- [x] **Ready**
  Ready (straighten) any exhausted (sideways) cards that you have in play.
- [x] **Upkeep**
  Get 1 gold for each of your workers.
  Also do anything that any card tells you to do during your upkeep.
-     **Main Phase**
  This is the bulk of your turn. You can do these things in any order:
  - [x] Hire a worker (maximum of once per turn)
  -     Build a tech building
  -     Build an add-on
  -     Summon your hero from your command zone
  -     Level up your hero if it’s in play
  - [ ] Play units from your hand
  - [ ] Play buildings from your hand
  - [ ] Play upgrades from your hand
  -     Play spells from hand
  - [ ] Activate abilities on cards already in play
  - [ ] Attack units
  -     Attack heroes
  - [ ] Attack buildings
  -     Attack Add-on buildings
  -     Attack Tech buildings
  - [ ] Attack Base building
  - [x] Lock in your patrol zone
- [x] **Draw Phase**
  Discard your entire hand face down. Next, draw the same number of cards from your draw pile that you just discarded, plus two more cards, but capped at 5 cards.
-     **Tech**
  Pick two cards from your codex to set aside. They’ll go face down into your discard pile just before your next turn starts.
  Teching two cards per turn is mandatory until you have 10 workers. At that point, you can tech 0, 1, or 2 cards. (Usually 0 is a good idea so you don’t bloat your deck.)

## Keywords/Triggers

_Checkboxes only for keywords used in starter deck cards._

- [ ] Anti-Air
- [ ] Armor
-     Armor Piercing
- [ ] Arrives
- [ ] Attacks
-     Boost
-     Channeling
-     Deathtouch
-     Detector
- [ ] Disable
-     Does
- [ ] End of Turn
-     Ephemeral
- [ ] Fading
- [ ] Flagbearer
- [ ] Flying
- [ ] Forecast
- [ ] Frenzy
- [ ] Haste
- [ ] Healing
- [ ] Illusion
- [ ] Indestructible
-     Invisible
-     Leaves
-     Legendary
-     Long-range
-     Max Level: Do This
-     Obliterate
- [ ] Overpower
- [ ] Resist
- [ ] Sacrifice
-     Sideline
- [ ] Sparkshot
- [ ] Stealth
- [ ] Summon
-     Swift Strike
-     Trash
-     Unattackable
- [ ] Unstoppable
- [ ] Untargetable
-     Upkeep: Do This
- [ ] While Patrolling

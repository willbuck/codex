module Codex
  class Game
    attr_reader :player_count

    def self.load(game_state)
      players = game_state['players'].keys
      self.new(players, game_state)
    end

    def initialize(players, game_state = {})
      @players = players
      @current_player = game_state['current_player']
      @game_state = game_state
    end

    def execute(command)
      player = @game_state['players'][@current_player]
      case command
      when 'READY'
        player['units'].each do |unit|
          # Untap
          unit.gsub!(', exhausted', '')
          # Remove from patrol zone
          unit.gsub!(/^Squad Leader /, '')
          unit.gsub!(/^Elite /, '')
          unit.gsub!(/^Scavenger /, '')
          unit.gsub!(/^Technician /, '')
          unit.gsub!(/^Lookout /, '')
        end

      when 'UPKEEP'
        player['gold'] = [
          20, @game_state['players'][@current_player]['gold'] + 4
        ].min

      when /^WORKER (.*)/
        player['workered'] ||= []
        if player['gold'] >= 1 and player['hand'].delete($1)
          player['gold'] -= 1
          player['workered'] << $1
          player['workers'] += 1
        end

      when /^PATROL (.*), (.*), (.*), (.*), (.*)/
        patrollers = [$1, $2, $3, $4, $5]
        if patrollers.all? { |patroller| patroller == 'empty' or player['units'].any?{|unit| unit.include? patroller }}
          slots = ["Squad Leader", "Elite", "Scavenger", "Technician", "Lookout"]
          patrollers.each do |patroller|
            next slots.shift if patroller == 'empty'
            player['units'].each do |unit|
              next unless unit.include? patroller
              unit.gsub!(/^/, slots.shift + " ")
              break
            end
          end
        end

      when 'DRAW'
        cards_in_hand = player['hand'].size
        cards_to_draw = [cards_in_hand + 2, 5].min
        cards_in_hand.times do
          player['discard'] << player['hand'].shift
        end
        cards_to_draw.times do
          player['deck'], player['discard'] = player['discard'].shuffle, [] if player['deck'].empty?
          player['hand'] << player['deck'].shift
        end

      when 'DONE'
        next_player = @players[(@players.index(@current_player)+1) % @players.size]
        @current_player = next_player
      end
    end

    def to_hash
      {
        'current_player' => @current_player,
        'players' => @game_state['players']
      }
    end
  end
end

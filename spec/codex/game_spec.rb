require_relative '../spec_helper'
require 'codex' # Load all the things, this is an integration spec

require 'yaml'

describe Codex do
  Dir['spec/scenarios/*.yml'].each do |file|
    describe File.basename(file, '.*') do
      YAML.load(File.read(file))['scenarios'].each do |scene|
        describe scene['description'] do
          Given(:game) { Codex::Game.load(scene['Given']) }

          When {
            scene['When'].each do |action|
              game.execute(action)
            end
          }

          thens = 0
          scene['Then'].each do |path, expected|
            thens += 1
            cmd = (thens == 1 ? :Then : :And)
            path, call = $1, $2 if path =~ /(.+)\.(.+)/
            path, index = $1, $2.to_i if path =~ /(.+)\[(\d+)\]/
            keys = path.split('/')
            if call
              send(cmd) { game.to_hash.dig(*keys).send(call) == expected }
            elsif index
              send(cmd) { game.to_hash.dig(*keys)[index] == expected }
            else
              send(cmd) { game.to_hash.dig(*keys) == expected }
            end
          end
        end
      end
    end
  end
end

$LOAD_PATH << 'lib'

require 'minitest/autorun'
require 'minitest/given'

Given.source_caching_disabled = false

require 'minitest/reporters'

Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

require 'pp'
